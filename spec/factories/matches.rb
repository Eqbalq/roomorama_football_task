FactoryGirl.define do
  factory :match do
    association :game, factory: :game
    association :winner_team, factory: :team
    score "1-1"
  end
end

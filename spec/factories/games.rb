FactoryGirl.define do
  factory :game do
    association :home_team, factory: :team
    association :away_team, factory: :team
    association :winner_team, factory: :team
  end
end



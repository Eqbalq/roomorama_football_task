FactoryGirl.define do
  factory :player do
    first_name Faker::Name.name
    last_name Faker::Name.name
    teams {[create(:team)]}
    user_id 1
  end
end

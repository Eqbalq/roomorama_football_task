require 'rails_helper'

RSpec.describe Team, type: :model do
  let!(:team) { create(:team) }
  let!(:home_game) { create(:game, home_team: team) }
  let!(:away_game) { create(:game, away_team: team) }

  it { should have_and_belong_to_many(:players) }
  it { should validate_presence_of(:name) }
  it { should have_many(:users).through(:players) }

  it { should have_many(:home_games) }
  it { should have_many(:away_games) }

  describe '#games' do
    it 'returns a list of home and away associated games.' do
      expect(team.games.size).to eq(2)

      expect(team.games).to include home_game
      expect(team.games).to include away_game
    end
  end

  describe '#home_games' do
    it 'returns only games played home' do
      expect(team.home_games.size).to eq(1)

      expect(team.home_games).to include home_game
    end
  end

  describe '#away_games' do
    it 'returns only games played home' do
      expect(team.away_games.size).to eq(1)

      expect(team.away_games).to include away_game
    end
  end

  describe 'played_game?' do
    it 'returns true if winner_team_id is set.' do
      home_game.winner_team = team
      expect(home_game.played_game?).to eq(true)
    end
  end
end

require 'rails_helper'

RSpec.describe Match, type: :model do
  let(:game) { create(:game) }

  it { should belong_to(:game) }
  it { should belong_to(:winner_team) }

  it { should validate_presence_of(:game) }
  it { should validate_presence_of(:score) }

  it 'validates the format of the score.' do
    %w(11-12 e-4 -6).each do |sc|
      expect{
        create(:match, score: sc.strip, game: game)
      }.to raise_error(
        ActiveRecord::RecordInvalid
      )
    end
  end
end

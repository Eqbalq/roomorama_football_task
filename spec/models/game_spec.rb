require 'rails_helper'

RSpec.describe Game, type: :model do
  let(:game) { create(:game) }

  it { should belong_to(:home_team) }
  it { should belong_to(:away_team) }
  it { should belong_to(:winner_team) }
  it { should have_many(:matches) }

  it { should validate_presence_of(:home_team) }
  it { should validate_presence_of(:away_team) }

  it 'wont allow more than three matches per game.' do
    matches = create_list(:match, 3, game: game)

    expect{ create(:match, game: game) }.to raise_error(
      ActiveRecord::RecordInvalid
    )
  end

  it 'raise an error if home and away teams are equal' do
    team = create(:team)
    game = build(:game,
      home_team: team,
      away_team: team
    )

    expect{ game.save! }.to raise_error(
      ActiveRecord::RecordInvalid
    )
  end
end

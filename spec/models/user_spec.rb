describe User do

  let(:user) { create(:user) }

  it { should respond_to(:email) }
  it { should have_many(:players) }

  it "#email returns a string" do
    expect(user.email).to match 'test@example.com'
  end

end

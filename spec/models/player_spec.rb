require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should belong_to(:user) }
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should have_and_belong_to_many(:teams) }

  it 'validates uniqueness of full name' do
    create(:player,
      first_name: "eki",
      last_name: "quran"
    )

    expect{
      create(:player,
        first_name: "eki",
        last_name: "quran"
      )
    }.to raise_error(
      ActiveRecord::RecordInvalid
    )
  end
end

include Warden::Test::Helpers
Warden.test_mode!

# Feature: User index page
#   As a user
#   I want to see a list of users
#   So I can see who has registered
feature 'Game index page', :devise do

  after(:each) do
    Warden.test_reset!
  end

  context "Valid admin user" do
    scenario 'List games' do
      admin = create(:user, :admin)

      games = create_list(:game, 3)

      login_as(admin, scope: :user)
      visit games_path
      expect(page).to have_content games.first.home_team.name
    end
  end

  context 'Not logged-in user' do
    scenario 'user sees access denied page.' do
      visit games_path
      expect(page).to have_content 'You need to sign in or sign up before continuing'
    end
  end
end

Rails.application.routes.draw do

  root to: 'visitors#index'
  devise_for :users
  resources :users
  resources :games
  resources :players
  resources :matches
  resources :teams
end

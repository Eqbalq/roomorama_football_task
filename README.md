Roomorama Football
================

  Ruby on Rails application which is part of an assignment for Roomorama.


  - Please note that usually I like to decouple/isolate application. API, business logic and workers each separate. I usually use some Rack based ruby mico-framework like Grape/Cuba to handle the API, while I move my business logic and models to a separate GEM. To keep this task simple I'll stick with using ROR.

  - I'm using RSpec with Capybara ( for unit and integration tests) with selenium-webdriver ( yes it's slow but who cares with this task :) .

  - I wont pay attention for performance at this stage ( no caching will be added for now ).

  - No Ajax (RJS) and visual effects added (for simplicity).

  - For simplicity I just used Service objects, but usually for production code I use Query, Value and Service objects.

  - I didn't pay any attention for the UX and to make it cool ( on the spot editing ) and ajaxify the forms for the same reason (no time)

  - Some integration tests should be still added (along with some controller tests to check out the routes) but for the same reasons this is not implemented.

  - Please note that I didn't use any of the admin tools like (ActiveAdmin) ..etc as I assumed you want to see my code rather than auto generate the code.

  - No enough time to create the statistics part of the task. Usually I use some caching (like elasticsearch) and do the calculation at the background and then update the statistics.



Design
------
![design](doc/models_complete.svg)


Technologies
-------------

- Ruby 2.2.2
- Rails 4.2.4
- RSpec
- Bootstrap
- authentication
- Devise
- authorization
- roles


Task
----

  Your task is to create a RoR application that will allow someone to do the following:

    - Create and manage users

    - Create games (using 2 - 4 team members)

    - Manage games (winner and score)

    - Show the game history

    - Show the stats of an individual player

Models
------

  Users
  -----

    Atrributes: first_name, last_name


  Team
  ----

    Has 1 or 2 people and play one match together

  Game
  ----

    Has a score from 0 - 10


  Match
  -----

    has one winning team

    Consists of 2 - 3 games (one team wins once they win 2 games)


  Optional Extras
  ---------------

    Show the best combination of players

    Show percentages (Win loss, best partner etc)

  Other
  -----

    Unit Tests

    Design is not the most important, but bonus points for a usable interface

  Contact us on developers@roomorama.com if you have any questions

  Please keep a good git log so we can see the progression of the code


Test account
---------------

  Here is the admin username and password for testing:

  ```
  admin_email: user@example.com
  admin_password: changeme
  ```

  * Don't forget to load the seeds data `bundle exec rake db:seed`.


Running the tests
---------------

  ```
  bundle exec spec .
  ```
class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :game_id
      t.integer :winner_team_id
      t.string :score

      t.timestamps null: false
    end
    add_index :matches, :game_id
    add_index :matches, :winner_team_id
  end
end

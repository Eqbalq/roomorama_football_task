class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, :except => :show

  def index
    @game   = Game.new
    @games  = Game.all.order(created_at: :desc).page params[:page]
    @teams  = Team.all_uniq_teams_mapped
  end

  def create
    @game = Game.new(game_params)

    if @game.save
      flash[:notice] = 'Game was successfully created.'
    else
      flash[:alert] = @game.errors.full_messages.inspect
    end

    redirect_to games_path
  end

  def show
    @game  = Game.find(params[:id])
    @match = Match.new(game: @game)
  end

  def destroy
    @game = Game.find(params[:id])
    @game.destroy
    redirect_to games_path, :alert => "Game deleted."
  end

  private

  def game_params
    params.require(:game).permit(:home_team_id, :away_team_id)
  end
end

class PlayersController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, :except => :show

  def index
    @player   = Player.new
    @players  = Player.all.order(created_at: :desc).page params[:page]
  end

  def create
    @player = Player.new(player_params)
    @player.user = current_user

    if @player.save
      flash[:notice] = 'player was successfully created.'
    else
      flash[:alert] = @player.errors.full_messages.inspect
    end

    redirect_to players_path
  end

  def destroy
    @player = Player.find(params[:id])
    @player.destroy
    redirect_to players_path, :alert => "Player deleted."
  end

  private

  def player_params
    params.require(:player).permit(:first_name, :last_name)
  end
end

class TeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, :except => :show

  def index
    @team    = Team.new
    @teams   = Team.all.order(created_at: :desc).page params[:page]
    @players = Player.all_uniq_players_mapped
  end

  def create
    @team = Team.new(team_params)

    @team.players = extract_players

    if @team.save
      flash[:notice] = 'team was successfully created.'
    else
      flash[:alert] = @team.errors.full_messages.inspect
    end

    redirect_to teams_path
  end

  def destroy
    @team = Team.find(params[:id])
    @team.destroy
    redirect_to teams_path, :alert => "Team deleted."
  end

  private

  def team_params
    params.require(:team).permit(:name)
  end

  def extract_players
    players = params[:team][:players]
    no_empty_players = players.reject { |c| c.empty? }
    Player.find(no_empty_players)
  end
end

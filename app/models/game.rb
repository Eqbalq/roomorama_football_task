class Game < ActiveRecord::Base
  belongs_to :home_team,
    class_name: "Team",
    foreign_key: "home_team_id"

  belongs_to :away_team,
    class_name: "Team",
    foreign_key: "away_team_id"

  belongs_to :winner_team,
    class_name: "Team",
    foreign_key: "winner_team_id"

  has_many :matches

  validates_length_of :matches, maximum: 3

  validates :home_team, presence: true
  validates :away_team, presence: true

  validate :check_teams_are_not_the_same

  # For pagination.
  paginates_per 10

  def played_game?
    !winner_team_id.nil?
  end

  private

  def check_teams_are_not_the_same
    if home_team_id == away_team_id
      errors.add(:away_team, "can't be the same as home_team")
    end
  end
end

class Player < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :teams

  validates_presence_of :first_name, :last_name,
    :message => "can't be blank"

  validates_uniqueness_of :first_name, scope: :last_name

  paginates_per 10

  def self.all_uniq_players_mapped
    all.uniq.map do |t|
      [t.full_name, t.id]
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

end

class Team < ActiveRecord::Base
  has_and_belongs_to_many :players
  has_many :users, through: :players

  has_many :home_games,
    class_name: "Game",
    foreign_key: :home_team_id

  has_many :away_games,
    class_name: "Game",
    foreign_key: :away_team_id

  validates_presence_of :name

  validates_length_of :players, maximum: 2

  paginates_per 10

  def self.all_uniq_teams_mapped
    all.uniq.map do |t|
      [t.name, t.id]
    end
  end

  def games
    [home_games + away_games].flatten.uniq
  end


end

class Match < ActiveRecord::Base
  belongs_to :game
  belongs_to :winner_team, class_name: "Team"

  validates :game, presence: true
  validates :score, presence: true

  validates :score,
    format: {
      with: /\A[0-9]-[0-9]\z/i
    }

  validate :validate_number_of_matched_per_game

  private

  # This is BAD. This should be moved to a service/query object
  def validate_number_of_matched_per_game
    return false if game.nil?
    if game.matches.count > 2
      errors.add(:game, "Max number of games allowed is 3")
    end
  end
end
